var Boss = Mob.extend({
    ctor: function( difficulty ) {
        this._super();
        this.difficulty = difficulty;
        this.initDefaultComponents( name );
        this.initSpecificComponents( difficulty );
    },
    
    initDefaultComponents: function( name ) {
        this.setHealth();
        this.x = this.getPositionX();
        this.y = this.getPositionY();
        this.initWithFile( 'res/images/Mob.png' );
        this.lineOfSight = null;
        this.player = null;
        this.spottedPlayer = false;
        this.timeUntilMove = 0;
        this.hitRange = null;
        this.name = name;
        this.hitCooldown = 0;
    },
    
    initSpecificComponents: function() {
        this.setHealthOnDifficulty();
        this.hitWhen = Mob.HITINSECOND;
        this.speed = Mob.SPEED;
    },
    
    patternAttack: function() {
        console.lgo( 'Attacking...' );
    },
    
    createMovingAnimation: function() {
        var animation = new cc.Animation.create();
//        animation.addSpriteFrameWithFile( res.effect_slash_1_png );
//        animation.addSpriteFrameWithFile( res.effect_slash_2_png );
//        animation.addSpriteFrameWithFile( res.effect_slash_3_png );
//        animation.addSpriteFrameWithFile( res.effect_slash_4_png );
//        animation.setDelayPerUnit( 0.05 );
        return cc.Repeat.create( cc.Animate.create( animation ), 1 );
    },
    
    setHealthOnDifficulty: function() {
        switch( this.difficulty ) {
            case 1:
                this.healths = Boss.HEALTH.EASY;
                break;
            case 2:
                this.healths = Boss.HEALTH.MEDIUM;
                break;
            case 3:
                this.healths = Boss.HEALTH.HARD;
                break;
            case 4:
                this.healths = Boss.HEALTH.VERYHARD;
                break;
            case 5:
                this.healths = Boss.HEALTH.ENDLESS;
                break;
        }
    },
    
});

Boss.HEALTH = {
    EASY : 3,
    MEDIUM : 5,
    HARD : 8,
    VERYHARD : 25,
    ENDLESS : 100
};