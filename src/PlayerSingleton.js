var PlayerSingleton = ( function() {
    var playerSingleton;
    
    function createInstance() {
        var obj = new Player();
        return obj;
    }
    
    return {
      getInstance : function() {
        if( !playerSingleton ) {
            playerSingleton = createInstance();
        }
        return playerSingleton;
      },
        newInstance : function() {
            playerSingleton = new Player();
            return playerSingleton;
        }
    };
} )();

