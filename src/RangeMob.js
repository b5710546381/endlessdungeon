var RangeMob = Mob.extend({
    
    ctor: function( name ) {
        this._super( name );
        
    },
    
    update: function( dt ) {
        this.spotPlayer();
        this.patternAttack();
        this.hitCooldown -= dt;
    },
    
    initSpecificComponents: function() {
        console.log( 'init fireball keeper' );
        this.fireballs = [];
        this.schedule( this.warpRandomly, 4.5 );
    },
    
    patternAttack: function() {
        if( this.spottedPlayer ) {
            if( this.hitCooldown <= 0 ) {
//                console.log( this.name+' is ready to hit.');
                this.hitFromRange();
//                var randomWarp = Math.floor( ( Math.random() * 8 ) + 1 );
//                console.log( 'Warp @ Node: '+randomWarp );
//                this.warpRandomly( randomWarp );
            }
        }
//        if( this.fireballs.length > 0 ) {
//            for( var i = 0; i < this.fireballs.length; i++ ) {
//                this.fireballs[ i ].checkHitPlayer();
//            }
//        }
        this.moveProjectiles();
    },
    
    getMobLineofSight: function() {
        var bottomLeftPointX = this.getPositionX() - RangeMob.SIGHTLENGTH;
        var bottomLeftPointY = this.getPositionY() - RangeMob.SIGHTLENGTH;
        return cc.rect( bottomLeftPointX, bottomLeftPointY, RangeMob.SIGHTLENGTH * 2, RangeMob.SIGHTLENGTH * 2 );
    },
    
    hitFromRange: function() {
        console.log( 'shoot a fireball' );
        var distanceX = this.getDistanceXFromPlayer();
        var distanceY = this.getDistanceYFromPlayer();
        var quadrant = this.checkPlayerQuadrant( this.player, distanceX, distanceY );
        var degree = Math.atan( distanceX / distanceY );
        if( quadrant == Mob.FOLLOWBYQUADRANTS.THIRDQUADRANT || quadrant == Mob.FOLLOWBYQUADRANTS.FOURTHQUADRANT ) degree += Math.PI;
        this.createFireball( degree );
//        this.schedule( this.warpRandomly, 4.5 );
        this.hitCooldown = RangeMob.HITCOOLDOWN;
    },
    
    warpRandomly : function() {
        var randomWarp = Math.floor( ( Math.random() * 8 ) + 1 );
        switch( randomWarp ) {
            case 1:
                this.setPosition( RangeMob.WARPPOINT.NODE_1 );
                break;
            case 2:
                this.setPosition( RangeMob.WARPPOINT.NODE_2 );
                break;
            case 3:
                this.setPosition( RangeMob.WARPPOINT.NODE_3 );
                break;
            case 4:
                this.setPosition( RangeMob.WARPPOINT.NODE_4 );
                break;
            case 5:
                this.setPosition( RangeMob.WARPPOINT.NODE_5 );
                break;
            case 6:
                this.setPosition( RangeMob.WARPPOINT.NODE_6);
                break;
            case 7:
                this.setPosition( RangeMob.WARPPOINT.NODE_7 );
                break;
            case 8:
                this.setPosition( RangeMob.WARPPOINT.NODE_8 );
                break;
        }    
    },
    
    createFireball: function( degree ) {
        var fireball = new cc.Sprite( res.ball_png );
        var owner = this;
        fireball.owner = owner ;
        fireball.degree  = degree;
        fireball.speed = RangeMob.FIREBALLSPEED;
        fireball.setPosition( this.x, this.y );    
        fireball.checkHitPlayer = function() {
            if( cc.rectContainsPoint( this.owner.player.getPlayerRect(), this.getPosition() ) ) {
                this.owner.player.handleHitFromMob( this.owner ) ;
                this.removeFromParent();
            }
        }
        fireball.update = function( dt ) {
            fireball.checkHitPlayer();
        } 
        fireball.scheduleUpdate();
        this.fireballs.push( fireball );
        this.getParent().addChild( fireball );
    },
    
    moveProjectiles: function() {
        for( var i = 0; i < this.fireballs.length; i++ ) {
            var fireball = this.fireballs[i];
            var fireballPosition = fireball.getPosition();
            var positionX = fireballPosition.x - ( fireball.speed * Math.sin( fireball.degree ) );
            var positionY = fireballPosition.y - ( fireball.speed * Math.cos( fireball.degree ) );
            fireball.setPosition( cc.p( positionX, positionY ) );
        }
    }
    
}); 

RangeMob.HITCOOLDOWN = 4;
RangeMob.SIGHTLENGTH = 300;
RangeMob.FIREBALLSPEED = 4;
RangeMob.WARPPOINT = {
    NODE_1 : cc.p( 200, 150 ),
    NODE_2 : cc.p( 200, 300 ), 
    NODE_3 : cc.p( 200, 450 ),
    NODE_4 : cc.p( 400, 150 ),
    NODE_5 : cc.p( 400, 300 ),
    NODE_5 : cc.p( 400, 450 ),
    NODE_6 : cc.p( 600, 150 ),
    NODE_7 : cc.p( 600, 300 ),
    NODE_8 : cc.p( 600, 450 )
}