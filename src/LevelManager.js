var LevelManager = cc.Layer.extend({
    init : function() {
        this._super();
        this.setPosition( cc.p( 0, 0 ) );
//        this.player = new Player();
        this.player = PlayerSingleton.getInstance();
        this.stageLevel = 1;
//        this.gameLayer = new GameLayer( this.player );
        this.gameLayer = new GameLayer();
//        this.player = this.gameLayer.player;
        this.gameLayer.setTag( 'layer' );
        this.gameLayer.init();
        this.addChild( this.gameLayer );
        this.nextLevelDoor = null;
        this.scheduleUpdate();
        this.hasCreateNextLevelDoor = false;
        this.floor = 1;
    },
    
    update : function() {
        this.handleStageComplete();
        this.checkEnterDoor();
    },
    
    createNextLevelDoor: function() {
        var door = this.getChildByTag( 'layer' ).getChildByTag( 'door' );
        door.runAction( door.animation );
        return cc.rect( 400 - 60, 600 - 50, 60, 60 );
    },
    
    checkEnterDoor : function() {
        if( this.hasCreateNextLevelDoor ) {
            var playerRect = this.gameLayer.player.getPlayerRect();
            var doorRect = this.nextLevelDoor;
            if( cc.rectOverlapsRect( playerRect, doorRect ) ) {
//                var player =  this.gameLayer.player;
                this.hasCreateNextLevelDoor = false;
                this.removeAllChildren();
                this.gameLayer.removeAllChildren();
//                this.gameLayer = new GameLayer( player );
                this.gameLayer = new GameLayer();
//                this.gameLayer.player = this.player;
                this.gameLayer.setTag( 'layer' );
                this.gameLayer.init();
                this.addChild( this.gameLayer );
            }
        }
    },
    
    handleStageComplete : function() {
        if( !this.hasCreateNextLevelDoor ) {
            if( this.player.knownMobs.length <= 0 ) {
                this.nextLevelDoor = this.createNextLevelDoor();
                this.hasCreateNextLevelDoor = true;
            }
        }
    },
    
    showGameover : function() {
        var gameOverLayer = new cc.LayerColor();
        gameOverLayer.setColor( new cc.Color( 50, 50, 50 ) );
        var gameOverLabel = new cc.LabelTTF( "GAME OVER", "Consolas", 60 );
        gameOverLabel.setPosition( cc.p( 400, 300 ) );
        var gameOverScore = new cc.LabelTTF( "You have scored "+this.player.score+" kills.", "Consolas", 40 );
        gameOverScore.setPosition( cc.p (400, 200 ) );
//        var restartLabel_1 = new cc.LabelTTF( "RESTART", "Consolas", 40 );
//        restartLabel_1.setColor( cc.color( 255, 255, 255 ) );
//        var restartLabel_2 = new cc.LabelTTF( "RESTART", "Consolas", 40 );
//        restartLabel_2.setColor( cc.color( 0, 0, 0 ) );
//        var onClicked = function() {
//            cc.director.resume();
//            this.gameLayer.removeAllChildren();
//            this.removeAllChildren();
//            cc.reload();
//            cc.director.runScene( new MenuScene() );
//        };
//        var restartItem = new cc.MenuItemSprite( restartLabel_1, restartLabel_2, function() {
//            cc.director.resume();
//            cc.director.runScene( new GameScene() );
//        }, true  ) );
//        var restartItem = new cc.MenuItemSprite(  restartLabel_1, restartLabel_2 , onClicked, this );
//        var restartItem = new cc.MenuItemSprite(  new cc.Sprite( res.start1_png ), new cc.Sprite( res.start2_png ), onClicked, this );
//        var menu = new cc.Menu( restartItem );
//        menu.setPosition( cc.p( 400, 100 ) );
        this.addChild( gameOverLayer );
        this.addChild( gameOverLabel );
        this.addChild( gameOverScore );
//        this.addChild( menu );
    },
    
    handleGameoverState : function() {
        this.showGameover();
    },
    
    showRemainingHealth: function() {
        var healthPositionX = 50;
        var healthPositionY = 800 - 50;
        var playerHealths = this.player.healths;
        for( var i = 0; i < playerHealths; i++ ) {
            var health = new cc.Sprite( res.health_png );
            this.setPosition( healthPositionX, healthPositionY - ( i * 50 ) );
            this.addChild( health );
        }
    }
    
});

var GameScene = cc.Scene.extend({
    onEnter: function() {
        this._super();
        var layer = new LevelManager();
        layer.init();
        this.addChild( layer );
    }
});