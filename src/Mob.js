var Mob = cc.Sprite.extend({
    ctor: function( name ) {
        this._super();
        this.initDefaultComponents( name );
        this.initSpecificComponents();
    },
    
    initDefaultComponents: function( name ) {
        this.setHealth();
        this.x = this.getPositionX();
        this.y = this.getPositionY();
        this.initWithFile( 'res/images/Mob.png' );
        this.lineOfSight = null;
        this.player = null;
        this.spottedPlayer = false;
        this.timeUntilMove = 0;
        this.hitRange = null;
        this.name = name;
        this.hitCooldown = 0;
    },
    
    initSpecificComponents: function() {
        this.hitWhen = Mob.HITINSECOND;
        this.speed = Mob.SPEED;
    },
    
    setHealth: function() {
        var random = Math.floor( Math.random() * 4 ) + 1;
        if( random > 1 ) this.health = Mob.MAXHEALTH;
        else this.health = 2;
        this.maxHealth = this.health;
    },
    
    update: function( dt ) {
        this.spotPlayer();
        this.hitRange = this.getMobHitRange();
        this.timeUntilMove += dt; 
        this.setPosition( this.x, this.y );
        this.patternAttack();
        this.hitCooldown -= dt;
    },
    
    patternAttack: function() {
        if( this.hitCooldown <= 0 ) {
//            console.log( this.name+' is ready to hit.');
            this.hitPlayer();
        } else {
            this.followPlayer();
        }
    },
    
    checkPlayerQuadrant: function( player, distanceX, distanceY ) {
                
        if( distanceX >= 0 ) {
            if( distanceY >= 0 ) {
                return Mob.FOLLOWBYQUADRANTS.FIRSTQUADRADNT;
            } else return Mob.FOLLOWBYQUADRANTS.FOURTHQUADRANT;
        } else {
            if( distanceY >= 0 ) {
                return Mob.FOLLOWBYQUADRANTS.SECONDQUADRANT;
            } else return Mob.FOLLOWBYQUADRANTS.THIRDQUADRANT;
        }
        
    },
    
    getDistanceXFromPlayer: function( player ) {
        var playerPositionX = this.player.getPositionX();
        var distanceX = this.getPositionX() - playerPositionX;
        return distanceX;
    },
    
    getDistanceYFromPlayer: function( player ) {
        var playerPositionY = this.player.getPositionY();
        var distanceY = this.getPositionY() - playerPositionY;
        return distanceY;
    },
    
    followPlayer: function() {
        if( this.spottedPlayer ) {
            if( this.timeUntilMove > 0.5 ) {
                var distanceX = this.getDistanceXFromPlayer();
                var distanceY = this.getDistanceYFromPlayer();
                var quadrant = this.checkPlayerQuadrant( this.player, distanceX, distanceY );
                var degree = Math.atan( distanceX / distanceY );
                
                if( quadrant == Mob.FOLLOWBYQUADRANTS.THIRDQUADRANT || quadrant == Mob.FOLLOWBYQUADRANTS.FOURTHQUADRANT ) degree += Math.PI;
                this.moveAfterPlayer( degree );
                this.timeUntilMove = 0;
            }
        }
    },
    
    moveAfterPlayer: function( degree ) {
        this.x -= this.speed * Math.sin( degree );
        this.y -= this.speed * Math.cos( degree );
    },
    
    spotPlayer: function() {
        if( !this.spottedPlayer ) ;
        if(  cc.rectOverlapsRect( this.lineOfSight, this.player.getPlayerRect() ) ) {
            this.spottedPlayer = true;
        }
    },

    hitPlayer: function() { 
        if( this.checkPlayerWithinHitRange() ) {
            this.player.handleHitFromMob( this );
            this.speed = 0;
        }
        this.hitCooldown = Mob.HITCOOLDOWN;
        this.speed = Mob.SPEED;
    },
    
    checkPlayerWithinHitRange: function() {
        return cc.rectOverlapsRect( this.hitRange, this.player.getPlayerRect() );
    },
    
    getMobHitRange: function() {
        var centerX = this.x;
        var centerY = this.y;
        return cc.rect( centerX - 50, centerY - 50, Mob.HITLENGTH, Mob.HITLENGTH );
    },
    
    getMobLineofSight: function() {
        var bottomLeftPointX = this.getPositionX() - 150;
        var bottomLeftPointY = this.getPositionY() - 150;
        return cc.rect( bottomLeftPointX, bottomLeftPointY, Mob.SIGHTLENGTH,Mob.SIGHTLENGTH );
    },
    
    setKnownPlayer: function( player ) {
        this.player = player;
    },
    
    handleHitFromPlayer: function( index ) {
        this.health -= 1;
        console.log( 'Mob has '+this.health+' left' );
        if( this.health <= 0 ) {
            this.player.score += this.maxHealth;
            this.removeFromParent();
            this.player.knownMobs.splice( index, 1 );
//            this.getParent().mobs.splice( index, 1 );
//            this.getParent().removeChild( this );
        }
    }
    
});

Mob.SPEED = 10;
Mob.SIGHTLENGTH = 250;
Mob.MOVEAFTERTWOSECONDS = 2; 
Mob.FOLLOWBYQUADRANTS = {
    FIRSTQUADRADNT: 1,
    SECONDQUADRANT: 2,
    THIRDQUADRANT: 3,
    FOURTHQUADRANT: 4
};
Mob.HITLENGTH = 100;
Mob.HITCOOLDOWN = 2.5;
Mob.HITINSECOND = 2;
Mob.MAXHEALTH = 1;