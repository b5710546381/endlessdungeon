    var GameLayer = cc.LayerColor.extend({
    init: function() {
        this._super( new cc.Color( 127, 127, 127, 255 ) );
        this.setPosition( cc.p( 0, 0) );
        this.mobs = [];
//        this.createStage();
        this.createTileStage();
        this.createPlayer();
        this.createMobs( 3  );
        this.addKeyboardHandlers();
    },
    
    createStage : function() {
        this.Stage = new cc.Sprite( res.Stage_png );
        this.Stage.setPosition( cc.p( 400, 300 ) );
        this.addChild( this.Stage );
    },
        
    createTileStage : function() {
        
        this.Stage = new Stage();
        this.Stage.setPosition( cc.p( 0, 0 ) );
        this.addChild( this.Stage );
        
        this.door = new cc.Sprite( res.stage_door1_png );
        this.door.animation = this.createDoorOpenAnimation();
        this.door.setTag( 'door' );
        
        this.addChild( this.door );
        this.door.setPosition( cc.p( 400, 600 - 30 ) );
        
    },
    
    createDoorOpenAnimation: function() {
        var animation = new cc.Animation.create();
        animation.addSpriteFrameWithFile( res.stage_door1_png );
        animation.addSpriteFrameWithFile( res.stage_door2_png );
        animation.addSpriteFrameWithFile( res.stage_door3_png );
        animation.addSpriteFrameWithFile( res.stage_door4_png );
        animation.setDelayPerUnit( 0.2 );
        return cc.Repeat.create( cc.Animate.create( animation ), 1 );
    },    
        
    createPlayer: function() {
//        this.player = new Player();
//        if( player == undefined ) 
        this.player = PlayerSingleton.getInstance();
//       else 
//           this.player = player;
//        console.log( this.player );
        this.player.setPosition( cc.p( 400, 50 ) );
        this.player.scheduleUpdate();
        this.addChild( this.player );
        this.player.createHealthBar();
        this.player.createScoreBar();
    },
        
    createMobs : function( total ) {
        for( var i = 0; i < total; i++ ) {
            var randomMob = Math.floor( Math.random() * 3 );
            var mobName = "Mob#" + ( i + 1 );
            if( randomMob == 1 ) var mob = new RangeMob( mobName );
            else var mob = new Slime();
//            else var mob = new Mob( mobName );
            this.mobs.push( mob );
            this.mobs[ i ].setPosition( 200 + (i * 200), 500 );
        }
        
        for( var i = 0; i < this.mobs.length; i++ ) {
            this.mobs[ i ].setKnownPlayer( this.player );
            this.mobs[ i ].lineOfSight = this.mobs[ i ].getMobLineofSight();
            this.mobs[ i ].hitRange = this.mobs[ i ].getMobHitRange();
            this.mobs[ i ].scheduleUpdate();
            this.addChild( this.mobs[ i ] );
            this.player.addKnownMobs( this.mobs[ i ] );
        }
        
//        this.mobsAlive = this.mobs.length;
        
    },
        
    addKeyboardHandlers : function() {
        var self = this;
        cc.eventManager.addListener({
            event: cc.EventListener.KEYBOARD,
            onKeyPressed: function( KeyCode, event ) {
                Player.MOVE_DIR[ KeyCode ] = true;
                Player.MOVING = true;
                Player.numOfMovingDirection++;
            },
            onKeyReleased: function( KeyCode, event ) {
                Player.MOVE_DIR[ KeyCode ] = false;
                Player.MOVING = false;
                Player.numOfMovingDirection--;
            }
        }, this);
    },
    
});

var StartScene = cc.Scene.extend({
    onEnter: function() {
        this._super();
        var layer = new GameLayer();
        layer.init();
        this.addChild( layer );
    }
});

GameLayer.STATE = {
    STOP : 0,
    START : 1
};