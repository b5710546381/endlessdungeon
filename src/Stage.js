var Stage = cc.Node.extend({
	ctor: function() {
		this._super();
		this.WIDTH = 16;
		this.HEIGHT = 12;
		this.STAGE = [
			
			'################',
			'################',
			'################',
			'################',
			'################',
			'################',
			'################',
			'################',
			'################',
			'################',
			'################',
            '++++++++++++++++'
		];
        
		this.generateStage();
	},
    
	generateStage: function() {
		for( var row = 0; row < this.HEIGHT ; row++ ) {
			for( col = 0; col < this.WIDTH ; col++ ) {
				if ( this.STAGE[row][col] == '+' ) {
					var heightPerBlock = 50;
					var centerEachBlock = 25;
                    var block = cc.Sprite.create( res.brick_png );
//					var block = cc.Sprite.create( res.Block_png );
					block.setAnchorPoint( cc.p( 0, 0 ));
					block.setPosition( cc.p( col*heightPerBlock, (row)* heightPerBlock ) );
					this.addChild(block);
				} else {
                    var heightPerBlock = 50;
					var centerEachBlock = 25;
                    var block = cc.Sprite.create( res.brick2_png );
//					var block = cc.Sprite.create( res.Block_png );
					block.setAnchorPoint( cc.p( 0, 0 ));
					block.setPosition( cc.p( col*heightPerBlock, (row)* heightPerBlock ) );
					this.addChild(block);
                }
			}
		}
	}
    
});