var MenuLayer = cc.LayerColor.extend({
    init: function() {
        this._super( new cc.Color( 127, 127, 127, 255 ) );
        this.setPosition( cc.p( 0, 0) );
        var background = new cc.Sprite( res.screen_png );
        background.setAnchorPoint( 0, 0 );
        background.setPosition( cc.p( 0, 0 ) );
        this.addChild( background );
        var winsize = cc.director.getWinSize();
        var centerpos = cc.p( winsize.width / 2, winsize.height / 2 );
//        var spriteTitle = new cc.Sprite();
//        spriteTitle.setPosition( centerpos );
//        this.addChild( spriteTitle );
        
        var startLabel_1 = new cc.LabelTTF( "START GAME", "Consolas", 40 );
        startLabel_1.setColor( cc.color( 250, 250, 250 ) );
        var startLabel_2 = new cc.LabelTTF( "START GAME ", "Consolas", 40 );
        startLabel_2.setColor( cc.color( 200, 200, 200 ) );
        var menuItemPlay = new cc.MenuItemSprite( startLabel_1, startLabel_2, this.onClicked, true );
//        var menuItemPlay = new cc.MenuItemSprite(  new cc.Sprite( res.start1_png ), new cc.Sprite( res.start2_png ), this.onClicked, true );
        menuItemPlay.setScale( 0.75 );
        var menu = new cc.Menu( menuItemPlay );
        menu.setPosition( centerpos );
        this.addChild( menu );
    },
    
    onClicked: function() {
        PlayerSingleton.newInstance;
        cc.director.resume();
        cc.director.runScene( new GameScene() );
    }
    
}); 

var MenuScene = cc.Scene.extend({
    onEnter: function() {
        this._super();
        var layer = new MenuLayer();
        layer.init();
        this.addChild( layer );
    }
});