var Slime = Mob.extend({
    ctor : function( name ) {
        this._super();
        this.createMainSprite();
        this.initDefaultComponents( name );
        this.initSpecificComponents();
    },
    
    initDefaultComponents: function( name ) {
        this.setHealth();
        this.x = this.getPositionX();
        this.y = this.getPositionY();
        this.mobStyle = Math.floor( Math.random() * 3 );
        this.lineOfSight = null;
        this.player = null;
        this.spottedPlayer = false;
        this.timeUntilMove = 0;
        this.hitRange = null;
        this.hitCooldown = 0;
    },
    
    initSpecificComponents: function() {
        this.prepareAnimation();
        this.hitWhen = Mob.HITINSECOND;
        this.speed = Mob.SPEED;
    },
    
    update : function( dt ) {
        this.spotPlayer();
        this.hitRange = this.getMobHitRange();
        this.timeUntilMove += dt; 
        this.setPosition( this.x, this.y );
        this.patternAttack();
        this.hitCooldown -= dt;
    },
    
    patternAttack: function() {
        if( this.hitCooldown <= 0 ) {
//            console.log( 'New slime '+this.mobStyle+' is ready to hit.');
            this.hitPlayer();
        } else {
            this.followPlayer();
        }
    },
    
    prepareAnimation: function() {
        this.straightAnimation = this.createStraightAnimation();
        this.hitAnimation = this.createHitAnimation();
        this.runAction( this.straightAnimation );
    },
    
    createMainSprite: function() {
        switch(  this.mobStyle ) {
            case 0:
                this.initWithFile( res.mob_b_slime1_png );
                break;
            case 1:
                this.initWithFile( res.mob_g_slime1_png );
                break;
            case 2:
                this.initWithFile( res.mob_r_slime1_png );
                break;
        }
    },
    
    createStraightAnimation: function() {
        var animation = new cc.Animation.create();
        switch( this.mobStyle ) {
            case 0:
                animation.addSpriteFrameWithFile( res.mob_b_slime1_png );
                animation.addSpriteFrameWithFile( res.mob_b_slime2_png );
                animation.addSpriteFrameWithFile( res.mob_b_slime3_png );
                break;
            case 1:
                animation.addSpriteFrameWithFile( res.mob_g_slime1_png );
                animation.addSpriteFrameWithFile( res.mob_g_slime2_png );
                animation.addSpriteFrameWithFile( res.mob_g_slime3_png );
                break;
            case 2:
                animation.addSpriteFrameWithFile( res.mob_r_slime1_png );
                animation.addSpriteFrameWithFile( res.mob_r_slime2_png );
                animation.addSpriteFrameWithFile( res.mob_r_slime3_png );
                break;
        }
        animation.setDelayPerUnit( 0.3 );
        return cc.RepeatForever.create( cc.Animate.create( animation ) );
    },
    
    createHitAnimation: function() {
        var animation = new cc.Animation.create();
        animation.addSpriteFrameWithFile( res.effect_hit1_png );
        animation.addSpriteFrameWithFile( res.effect_hit2_png );
        animation.addSpriteFrameWithFile( res.effect_hit3_png );
        animation.addSpriteFrameWithFile( res.effect_hit4_png );
        animation.addSpriteFrameWithFile( res.effect_hit5_png );
        animation.setDelayPerUnit( 0.1 );
        return cc.Repeat.create( cc.Animate.create( animation ), 1 );
    },
    
    hitPlayer: function() { 
        if( this.checkPlayerWithinHitRange() ) {
            
            var owner = this;
            var hitEffect = new cc.Sprite();
            hitEffect.owner = owner;
            hitEffect.setPosition( this.player.getPosition() );
            hitEffect.hitEffectAction = this.createHitAnimation();
            hitEffect.runAction( hitEffect.hitEffectAction );
            hitEffect.update = function( dt ) {
                if( this.hitEffectAction.isDone() ) {
                    this.removeFromParent();
                }
            };
            hitEffect.scheduleUpdate();
            this.getParent().addChild( hitEffect );
            this.player.handleHitFromMob( this );
            this.speed = 0;
            
        }
        this.hitCooldown = Mob.HITCOOLDOWN;
        this.speed = Mob.SPEED;
    },
    
});