var Player = cc.Sprite.extend({
    ctor: function() {
        this._super();
        this.initWithFile( res.swordman_idle_png );
        this.initPlayerProperties();
//        this.walkAction = this.createWalkAction();
        this.x = this.getPositionX();
        this.y = this.getPositionY();
        this.score = 0;
        this.initPlayerAnimations();
//        this.stamina = Player.STAMINA;
//        this.currentAtkAction = this.createAttackAction();
    },
    
    initPlayerAnimations: function() {
        this.walkAction = this.createWalkAction();
        this.currentAtkAction = this.createAttackAction();
        
    },
    
    initPlayerProperties: function() {
        
        this.speed = Player.INITIALSPEED;
        this.IsMoving = Player.MOVING;
        this.accelerationTime = 0;
        this.facing = 0;
        this.previousFacing = 0;
        this.knownMobs = [];
        this.healths = Player.HEALTH;
        this.state = Player.STATE.ALIVE;
        this.hitCooldown = 0;
        this.walkDuration = Player.WALKDURATION;
    },
    
    update: function( dt ) {
        if( this.state == Player.STATE.ALIVE ) {
//            this.movingWithAcceleration();
        
            this.moveFromKeyboardInputs( dt );
        
            this.accelerationTime += dt;
            
            if( this.hitCooldown <= 0 ) {
//                console.log( 'Ready to hit' );
                this.hitListener();
            }
            
            if( this.currentAtkAction.isDone() ) {
                this.speed = Player.INITIALSPEED;
                this.stopAction( this.currentAtkAction );
                for( var i = 0; i < this.knownMobs.length; i++ ) {
                    this.hit( this.knownMobs[i] , i );
                }
                this.currentAtkAction = this.createAttackAction();
            } 
            
            if( this.isDead() ) this.handleDeadState();
            
            this.hitCooldown -= dt;
            this.atkActionTime += dt;
            this.previousFacing = this.facing;
        }
    },
    
    createWalkAction : function() {
        var animation = new cc.Animation.create();
        animation.addSpriteFrameWithFile( res.swordman_walk1_png );
        animation.addSpriteFrameWithFile( res.swordman_walk2_png );
        animation.setDelayPerUnit( 0.5 );
        return cc.Repeat.create( cc.Animate.create( animation ), 1 );
    },
        
    createAttackAction : function() {
        var animation = new cc.Animation.create();
        animation.addSpriteFrameWithFile( res.swordman_atk1_png );
        animation.addSpriteFrameWithFile( res.swordman_atk2_png );
        animation.addSpriteFrameWithFile( res.swordman_atk3_png );
        animation.addSpriteFrameWithFile( res.swordman_atk4_png );
        animation.setDelayPerUnit( 0.05 );
        return cc.Repeat.create( cc.Animate.create( animation ), 1 );
    },
    
    createSlashAction : function() {
        var animation = new cc.Animation.create();
        animation.addSpriteFrameWithFile( res.effect_slash_1_png );
        animation.addSpriteFrameWithFile( res.effect_slash_2_png );
        animation.addSpriteFrameWithFile( res.effect_slash_3_png );
        animation.addSpriteFrameWithFile( res.effect_slash_4_png );
        animation.setDelayPerUnit( 0.05 );
        return cc.Repeat.create( cc.Animate.create( animation ), 1 );
    },
    
    moveFromKeyboardInputs : function( dt ) {
//        if( Player.MOVE_DIR[ cc.KEY.shift ] ) {
//            if( this.stamina > 0 ) {
//                if( this.numOfMovingDirection > 2 ) this.speed = Math.sqrt( 2 ) * Player.MAXSPEED;
//                else this.speed = Player.MAXSPEED;
//                this.stamina -= dt;
//            } else this.speed = Player.SPEED;
//        } else if( this.stamina <= 5 ) this.stamina += dt;
        
        if( Player.MOVE_DIR[ cc.KEY.left ] ) {
            if( this.x > 50 ) this.x -= this.speed;
            this.facing = Player.FACINGDIRECTION.LEFT;
        }
        if( Player.MOVE_DIR[ cc.KEY.right ] ) {
            if( this.x < 750 ) this.x += this.speed;
            this.facing = Player.FACINGDIRECTION.RIGHT;
        }
        if( Player.MOVE_DIR[ cc.KEY.up ] ) {000
            if( this.y < 550 ) this.y += this.speed;
            this.facing = Player.FACINGDIRECTION.UP;
        }
        if( Player.MOVE_DIR[ cc.KEY.down ] ) {
            if( this.y > 50 ) this.y -= this.speed;
            this.facing = Player.FACINGDIRECTION.DOWN;
        }
        if( Player.MOVING ) this.handlePlayerMovement( this.facing );
    },
    
    handlePlayerMovement: function( facing ) {
        if( facing == Player.FACINGDIRECTION.LEFT ) {
            this.setRotation( 90 );
        } else if( facing == Player.FACINGDIRECTION.RIGHT ) {
            this.setRotation( 270 );
        } else if( facing == Player.FACINGDIRECTION.UP ) {
            this.setRotation( 180 );
        } else if( facing == Player.FACINGDIRECTION.DOWN ) {
            this.setRotation( 0 );
        }
        this.handleWalkAction();
    },    
        
    handleWalkAction: function() {
        if( this.getNumberOfRunningActions() == 0 ) {
            this.runAction( this.walkAction );
        }
    },
    
    hitListener : function() {
        if( Player.MOVE_DIR[ cc.KEY.z ] ) {
            if( this.knownMobs != null ) {
                this.runAction( this.currentAtkAction );
                this.speed = 0;
                this.hitCooldown = Player.HITCOOLDOWN;
            } else console.log( "can't hit" );
        }
    },
    
//    movingWithAcceleration : function() {
//        if( Player.MOVING ) {
//            this.runAction( this.walkAction );
//            if( Player.numOfMovingDirection > 1 ) {
//                if( this.speed < Player.MAXSPEED && this.accelerationTime >= Player.TIME_PER_ACCELERATE ) {
//                this.speed += Player.DIAGONALACCELERATION;
//                this.accelerationTime = 0;
//                }
//            } else {
//                if( this.speed < Player.MAXSPEED && this.accelerationTime >= Player.TIME_PER_ACCELERATE ) {
//                    this.speed += Player.ACCELERATION;
//                    this.accelerationTime = 0;
//                } 
//            }
//        } else {
//            this.speed = Player.INITIALSPEED;
//        }
//    },
    
    checkOverBoundary : function() {
        return this.x < 50 || this.x > 750 || this.y < 50 || this.y > 550;
    },
    
    getPlayerRect: function() {
        var spriteRect = this.getBoundingBoxToWorld();
        var spritePos = this.getPosition();

        var dX = this.x - spritePos.x;
        var dY = this.y - spritePos.y;
        return cc.rect( spriteRect.x + dX,
                        spriteRect.y + dY,
                        spriteRect.width,
                        spriteRect.height );
    },
    
    
    
    hit: function( mob, index ) {
        if( this.knownMobs.length <= 0 ) return;
        var mobPosition = mob.getPosition();
        switch ( this.facing ) {
            case 1:
                if( cc.rectContainsPoint( cc.rect( this.x - 100, this.y - 50, 100, 100 ), mobPosition ) ) {
                    this.hasMobtoHit = true;
                }
                break;
            case 2:
                if( cc.rectContainsPoint( cc.rect( this.x, this.y - 50, 100, 100 ), mobPosition ) ) {
                    this.hasMobtoHit = true;
                }
                break;
            case 3:
                if( cc.rectContainsPoint( cc.rect( this.x - 50, this.y, 100, 100 ), mobPosition ) ) {
                    this.hasMobtoHit = true;
                }
                break;
            case 4:
                if( cc.rectContainsPoint( cc.rect( this.x - 50, this.y - 100, 100, 100 ), mobPosition ) ) {
                    this.hasMobtoHit = true;
                }
                break;
        }
        if( this.hasMobtoHit ) {
            var slashAnimation = new cc.Sprite();
            var owner = this;
            slashAnimation.owner = owner;
            slashAnimation.setPosition( mob.getPosition() );
            slashAnimation.slashAction = this.createSlashAction();
            slashAnimation.runAction( slashAnimation.slashAction );
            slashAnimation.update = function( dt ) {
                if( this.slashAction.isDone() ) {
                    this.removeFromParent();
                    this.owner.removeMobAfterHit( mob, index );
                }
            };
            slashAnimation.scheduleUpdate();
            this.getParent().addChild( slashAnimation );
            this.hasMobtoHit = false;
        }
            
        console.log( 'Mobs left: '+this.knownMobs.length );
    },
    
    removeMobAfterHit : function( mob, index ) {
        this.score += mob.maxHealth;
        console.log( this.score );
//        mob.handleHitFromPlayer( index );
        this.getParent().removeChild( mob );
        this.knownMobs.splice( index, 1 );
        this.getParent().mobs.splice( index, 1 );
        this.hasMobtoHit = false;
    },
    
    addKnownMobs: function( mob ) {
        this.knownMobs.push( mob );
    },
    
    handleHitFromMob: function( mob ) {
        if( this.state == Player.STATE.ALIVE ) {
            console.log( "Crap! Got Hit by "+mob.name );
            this.decreaseHealth();
        }
    },
    
    isDead: function() {
        if( this.healths <= 0 ) {
            return true;
        }
    },
    
    handleDeadState: function() {
        console.log( 'GAME OVER' );
        this.state = Player.STATE.DEAD;
        this.getParent().getParent().handleGameoverState();
    },
    
    createHealthBar : function() {
//        console.log( 'Creating health bar...' );
        this.healthBar = [];
//        var healthPositionX = 800 - 50;
//        var healthPositionY = 50;
        var healthPositionX = 50;
        var healthPositionY = 600 - 25;
        for( var i = 0; i < this.healths; i++ ) {
//            var health = cc.Sprite.create( res.health_png );
            var health = cc.Sprite.create( res.heart_png );
            health.setPosition( healthPositionX + ( i * 45 ), healthPositionY );
            this.healthBar.push( health );
            this.getParent().addChild( health );
        }
    },
    
    createScoreBar : function() {
        console.log( 'Creating Score bar...' );
        var owner = this;
        var scoreSprite = new cc.Sprite();
        this.labelScore = new cc.LabelTTF('x 0', 'Consolas', 32 );
        this.labelScore.setColor( cc.color( 255, 255, 255 ) );
        this.labelScore.setPosition( cc.p( 700, 570 ) );
        this.labelScore.owner = owner;
        this.labelScore.update = function( dt ) {
            this.setString( 'X '+this.owner.score );
        };
        this.labelScore.scheduleUpdate();
        scoreSprite.addChild( this.labelScore );
        this.getParent().addChild( scoreSprite );
    },
    
    decreaseHealth: function() {
        this.healths -= 1;
        console.log( "Player has "+this.healths+" healths left." );
        var index = this.healths;
        this.getParent().removeChild( this.healthBar[ index ] );
        this.healthBar.splice( index, 1 );
    }
    
});

Player.HEALTH = 5;
Player.MOVE_DIR = [];
Player.INITIALSPEED = 2.5;
Player.MAXSPEED = 2.5;
Player.ACCELERATION = 0.25;
Player.TIME_PER_ACCELERATE = 0.1;
Player.MOVING = false;
Player.DIAGONALACCELERATION = 0.05;
Player.HITCOOLDOWN = 1.5;
Player.WALKDURATION = 1;
Player.STAMINA = 5;    

Player.numOfMovingDirection = 0;
    
Player.STATE = {
    ALIVE: 0,
    DEAD: 1
};
Player.FACINGDIRECTION = {
    LEFT: 1,
    RIGHT: 2,
    UP: 3,
    DOWN: 4
};

Player.ANGLE = {
    LEFT : 270,
    RIGHT : 90,
    UP : 180,
    DOWN : 0
};